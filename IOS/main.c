#include "main.h"

int client_fd;


//初始化所有内容
void init()
{
	//初始化登陆信息存储
	init_login();
	//初始化缓存
	init_cache();
	//初始化数据可
	init_sql();
	//队列
	init_queue();
	//数据加密初始化
	init_aes();
}

//初始化登陆信息存储
void init_login()
{
	//参考IOS登陆名存储   在这不实现
}
//初始化缓存
void init_cache()
{
	init_device();
	//init_device();
	
}
//初始化数据库
void init_sql()
{
	//根据IOS来做
}
// 队列
void init_queue()
{
	// Queue *queue;
	// queue = initQueue();
	
	// pthread_mutex_init(&queue_pop_mutex,NULL);
	// pthread_mutex_init(&queue_top_mutex,NULL);
	List *list = listCreate(); 
	open_phread(list);
	
}

// 数据加密初始化
void init_aes()
{
	aes_init();
}

//连接网络
void connect_web()
{
	client_fd = client_connect();
}
//线程
void open_phread(List *list)
{
	pthread_t receive_thread, deal_thread;
	pthread_create(&receive_thread, NULL, receive_data, (void *)list);
	pthread_create(&deal_thread,NULL,deal_data,(void *)list);
	
}


