#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>

#define ERR(s) do{fprintf(stderr,"[%s:%d]%s : %s\n",__FILE__,__LINE__,s,strerror(errno));exit(-1);}while(0)

	
#endif