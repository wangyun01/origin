#ifndef __RECEIVE_H__
#define __RECEIVE_H__

#include "aes_option.h"
#include "list.h"
#include "common.h"

  // 打开探测
#define     SOL_TCP      6
#define     TCP_KEEPIDLE 4
#define     TCP_KEEPINTVL 5
#define     TCP_KEEPCNT 6	// 发送探测分节的次数


#define     IDLE        60	// 如该连接在60秒内没有任何数据往来,则进行探测
#define     INTVL        3	 // 探测时发包的时间间隔为3 秒
#define     CNT          3	 // 探测尝试的次数.如果第1次探测包就收到响应了,则后2次的不再发.
#define		REC_BUFSIZE 	2048

//char web_rec_buf[REC_BUFSIZE] = "";
int sock_alive(int fd);		//判断套接字是否还在连通状态
void *receive_data(void *arg);		//接受数据

#endif
