#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <memory.h>
#include <pthread.h>
#include "list.h"
pthread_mutex_t mutex; 
	
	
List *listCreate()
{
	pthread_mutex_init(&mutex,NULL);
	
	//创建List的空间
	List *pList = malloc(sizeof(List));
	if(NULL == pList)
		return NULL;
	pList->size = 0;

	//创建头结点的空间
	pList->pHead = malloc(sizeof(Node));
	if(NULL == pList->pHead)
	{
		free(pList);
		return NULL;
	}
	memset(&(pList->pHead->data),0,sizeof(T));
	pList->pHead->next = NULL;
	return pList;
}

static Node *createNewNode(T data)
{
	Node *newNode = malloc(sizeof(Node));
	if(NULL == newNode)
		return NULL;
	//newNode->data = data;
	memcpy(&(newNode->data),&data,sizeof(T));
	newNode->next = NULL;
	return newNode;
}

int listPushBack(List *pList,T data)
{
	pthread_mutex_lock(&mutex);
	//1.创建新节点
	Node *newNode = createNewNode(data);
	if(NULL == newNode)
		return -1;

	//2.找到尾节点
	Node *pTemp = pList->pHead;
	while(pTemp->next)
		pTemp = pTemp->next;
	
	//3.将尾节点的next指向新节点
	pTemp->next = newNode;
	newNode->next = NULL;

	//4.list的size自增
	pList->size++;
	//printf("------------------------push\n");
	//printf("------------------------pList->size:%d\n",pList->size);
	pthread_mutex_unlock(&mutex);
	return 0;
}


int listPopFront(List *pList)
{
	//1.找到头结点
	Node *pHead = pList->pHead;

	//2.前一个指向后一个
	Node *pTemp = pHead->next;
	pHead->next = pTemp->next;

	//3.删除
	free(pTemp);

	//4.size--
	pList->size--;
	return 0;
}



int listFind(List *pList,int pos,T *pData)
{
	pthread_mutex_lock(&mutex);
	if(pos<0)
		pos = 0;
	if(pos>pList->size)
		pos = pList->size;

	//1.找到第pos个节点	
	Node *pTemp = pList->pHead;
	pTemp = pList->pHead;
	do{
		pTemp = pTemp->next;
	}while(pos--);
	
	//2.将第pos个节点的值赋值给结果参数pData
	*pData = pTemp->data;
	//printf("------------------------pop\n");
	//printf("------------------------pTemp->data:%s\n",pData->content);
	pthread_mutex_unlock(&mutex);
	return 0;
}

int listSize(List *pList)
{
	return pList->size;
}

int listEmpty(List *pList)
{
	return pList->size==0;
}

int listClear(List *pList)
{
	Node *pHead,*pTemp;
	pHead = pList->pHead;
	while(pHead->next)
	{
		pTemp = pHead->next;
		pHead->next = pTemp->next;
		free(pTemp);
		pList->size--;
	}
	return 0;
}

void listDestroy(List *pList)
{
	listClear(pList);	
	free(pList->pHead);
	free(pList);
}

