#include "analy_data.h"


//组合模式
void analy_group(char *buf)
{
	int i = 0,j = 0,digit = 26;
	int group_num = Char_to_int(buf + 4);
	//printf("====num_group%d ============\n",group_num);
	printf("===================GROUP=======================\n");
	//10种固定模式的使用
	for(j = 0;j < 10; j++ )
	{
		memset(&mode_status,0,sizeof(mode_status));
		mode_status[j] = Char_to_Byte(buf + j);
		printf("mode:%d\n",mode_status[j]);	
	}
	for(i = 0;i < group_num - 10;i++)
	{
		memset(&group_data,0,sizeof(group_data));
		group_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(group_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(group_data[i].area,buf + digit,area_len);
		digit += 12;
		group_data[i].type = Char_to_int(buf + digit);
		digit += 4;
		group_data[i].Contype = Char_to_int(buf + digit);
		digit += 4;
		printf("num : %d\tgroup_id:%d\tname:%s,area:%s,type:%d,Contype:%d\n",i ,group_data[i].id,group_data[i].name,group_data[i].area,group_data[i].type,group_data[i].Contype);	
	}
	
}

//灯信息
void analy_light(char *buf)
{
	int j=0,digit = 16;
	
	
	int light_num = Char_to_int(buf + 4);
	
	//printf("relay_num: %d,use_num :%d\n",relay_num,use_num);
	
	memset(&light_data,0,sizeof(light_data));
	light_data[light_num].id = Char_to_int(buf + digit);
	digit += 4;
	int use_num = Char_to_int(buf + digit);
	digit += 4;
	for(j = 0;j < use_num;j++)
	{
		light_data[light_num].port[j] = Char_to_int(buf + digit);
		digit += 4;
		
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		//printf("===========================================\n");
		memcpy(light_data[light_num].name[j] , buf + digit, name_len);
		printf("===================LIGHT=======================\n");
		digit += 12;
		
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(light_data[light_num].area[j] , buf + digit, area_len);
		digit += 12;
		printf("id %d port:%d  name:%s area:%s \n",light_data[light_num].id,light_data[light_num].port[j],light_data[light_num].name[j],light_data[light_num].area[j]);
		//light_num++;	
	
	}	
				
}
//继电器状态
void analy_relay_status(char *buf)
{
	int i = 0;
	int use_num = Char_to_int(buf + 4);
	for(i = 0 ;i < use_num;i++)
	{
		relay_status[i] = Char_to_Byte(buf + 16 + i);
	
	
	printf("relay_sta:%d\n",relay_status[i]);	

	}
}


//窗帘
void analy_curtain(char *buf)
{
	int i = 0,digit = 16;
	int curtain_num = Char_to_int(buf + 4);
printf("===================CURTAIN=======================\n");
	for(i = 0;i < curtain_num;i++)
	{
		curtain_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(curtain_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(curtain_data[i].area,buf + digit,area_len);
		digit += 12;
		printf("curtain_id:%d,name:%s,area:%s\n",\
		curtain_data[i].id,curtain_data[i].name,curtain_data[i].area);	
	}
	
}
//插座
void analy_socket(char *buf)
{
	printf("===================SOCKET=======================\n");
	int i = 0 ,digit = 16;
	int socket_num = Char_to_int(buf + 4);
	for(i = 0;i < socket_num; i++)
	{
		
		socket_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(socket_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(socket_data[i].area,buf + digit,area_len);
		digit += 12;
		printf("socket_id:%d,name:%s,area:%s\n",\
		socket_data[i].id,socket_data[i].name,socket_data[i].area);	
	}
	
}
//插座状态
void analy_socket_status(char *buf)
{
	int socket_num  = Char_to_int(buf + 4);
	int i = 0;
	for(i = 0;i < socket_num;i++)
	{
		socket_status[i] = Char_to_Byte(buf + 16 + i);
		printf("socket_sta:%d\n",socket_status[i]);
	}
	
}
//门锁
void analy_door(char *buf)
{
	int i = 0,digit = 16;
	//Door_Status door;
	int num_safelock = Char_to_int(buf + 4);
	
	printf("===================DOOR=======================\n");
	for(i = 0; i < num_safelock;i++)
	{
		door_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(door_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + 34);
		digit += 2;
		memcpy(door_data[i].area,buf + digit,area_len);
		printf("door_id:%d,name:%s,area:%s\n",\
		door_data[i].id,door_data[i].name,door_data[i].area);	
	}
	
	
}
//门锁状态
void analy_door_status(char *buf)
{
	int safe_lock_num = Char_to_int(buf + 4 );
	int i = 0,j = 0;
	for(i = 0;i < safe_lock_num;i++)
	{
		for(j = 0;j< i;j++)
		{
			door_status[i].status[j] = Char_to_Byte(buf + (16 + j));
			door_status[i].power[j] = Char_to_Byte(buf + (17 + j));
			j+=2;
			printf("door_sta:%d,door_power:%d\n",door_status[i].status[j],door_status[i].status[j]);
		}
	}
		
}
//家电信息
void app_analy(char *buf)
{
	int i = 0,j = 0,digit = 16;
	int App_num = Char_to_int(buf + 4);
	if (App_num == 0)
		printf("无此类信息\n");
	printf("===================APPLIANCE=======================\n");
	
	int app_count = Char_to_int(buf + 12);
	
	if(app_count != 0)
	{
		//printf("~~~~~~~~~~~~~~app_count:%d\n",app_count);
		for(j = 0;j < App_num; j++)
		{
			//printf("~~~~~~~~~~~~~~app_num:%d\n",App_num);
			memset(&app_data,0,sizeof(app_data));
			app_data[i].infrared_id = Char_to_int(buf + digit);
			digit += 4;
			app_data[i].app_id = Char_to_int(buf + digit);
			digit += 4;
			app_data[i].app_type = Char_to_int(buf + digit);
			digit += 4;
			int name_len = Short_to_int(buf + digit);
			digit += 2;
			memcpy(app_data[i].name,buf + digit,name_len);
			digit += 12;
			int area_len = Short_to_int(buf + digit);
			digit += 2;
			memcpy(app_data[i].area,buf + digit,area_len);
			digit += 12;
		}
		printf("infrared_id:%d,app_id:%d,type:%d,name:%s,area:%s\n",app_data[i].infrared_id,app_data[i].app_id,app_data[i].app_type,app_data[i].name,app_data[i].area);
		
		digit = 16;
	}
}

//背景音乐信息
void analy_audio(char *buf)
{
	// int i = 0;
	// int audio_num = Char_to_int(buf + 4);
	// int name_len = Short_to_int(buf + 20);
	// int area_len = Short_to_int(buf + 34);
	
	// for(i = 0;i < audio_num;i++)
	// {
		// audio_data[i].id = Char_to_int(buf + 16);
		// memcpy(audio_data[i].name,buf + 22,name_len);
		// memcpy(audio_data[i],area,buf + 36,area_len);
	// }
}

//背景音乐状态
void analy_audio_status(char *buf)
{
	audio_status.id = Char_to_int(buf + 16);
	audio_status.power = Char_to_int(buf + 20);
	audio_status.play_status = Char_to_int(buf + 24);
	audio_status.vol = Char_to_int(buf + 28);
	audio_status.play_mode = Char_to_int(buf + 32);
	audio_status.play_style = Char_to_int(buf + 36);
	memcpy(audio_status.current_music,buf + 40,100);
}

