#ifndef __SEND__H__
#define __SEND__H__

#include "common.h"
#include "aes_option.h"


#define		SEND_BUFSIZE 	2048



//登陆信息
void send_login(char *name , char *pass);
//注册信息
void send_region(char *name , char *pass , char *ctrl_pass,char *phone);
//修改密码信息
void change_login_info(char *name,char *old_pass,char *new_pass);
//forget password
void forget_password(char *name,char *pass,char *ctrl_pass);
//获取设备信息
void get_all_status(int type,int option);
//基本控制
void basic_contrl(int type,int id,int port,int status);
//门锁控制
void door_contrl(int id,int option,char *pass);
//背景音乐请求数据
void Get_Music_Data(int type,int request_type,int id,int data_type);
//背景音乐基本操作
void Music_Contrl(int type,int id,int option_type,int cmd1,int cmd2,int data1,int data2); 

//设置数据头部
void set_Head_Data(int type,int total , int note1, int note2);
//自定义数据类型装换
void Int_to_char(int location , int data);
//补充名称
void Name_padding(int location , int length);
#endif