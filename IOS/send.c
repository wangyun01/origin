#include "send.h"


extern int client_fd;
char web_send_buf[SEND_BUFSIZE]; 
//发送数据
int Web_Send_Data(int len)
{
	/***
	数据加密流程
	1、对发送数据加密
	2、加密后新数据和数据长度
	3、发送加密后数据长度
	4、发送加密数据
	*/
	 int count = 0;		//用来存储读取的数据长度
	// 1、数据加密
	char *encrypt_str = NULL;	//加密以后的数据
	aes_encode(web_send_buf, &encrypt_str , len);
	printf("web_send_buf: %s\n",web_send_buf);
	
	printf("web_send_buf: %s\n",web_send_buf + 48);
	
	printf("web_send_len: %d\n",len);
	if((count = write(client_fd,encrypt_str,strlen(encrypt_str))) < 0)
	{
		ERR("send faile");
	}
	free(encrypt_str);
	return count;		

}

//登陆信息
void send_login(char *name , char *pass)
{
	int digit = 16;
	int name_len = strlen(name);
	int pass_len = strlen(pass);
	memset(web_send_buf,0,sizeof(web_send_buf));
	set_Head_Data(21 ,1 ,name_len,pass_len);
	memcpy(web_send_buf + digit, name, 32);

	Name_padding(digit + strlen(name) , 32 - strlen(name));
	digit += 32;
	memcpy(web_send_buf + digit, pass, 32);
	
	Name_padding(digit + strlen(pass) , 32 - strlen(pass));
	digit += 32;
	printf("--------------------%s\n",web_send_buf);
	Web_Send_Data(16+2*32);
}
//注册信息
void send_region(char *name , char *pass , char *ctrl_pass,char *phone)
{
	int digit = 16;
	set_Head_Data(21,2,0,0);
	memcpy(web_send_buf + digit,name,32);
	
	Name_padding(digit + strlen(name),32 - strlen(name));
	
	digit += 32;
	memcpy(web_send_buf + digit,pass,32);
	Name_padding(digit + strlen(pass),32 - strlen(pass));
	
	digit += 32;
	memcpy(web_send_buf + digit,phone,11);
	
	digit += 11;
	//printf("digit %d\n",digit);
	memcpy(web_send_buf + digit,ctrl_pass,32);
	
	//Name_padding(digit + strlen(ctrl_pass),32 - strlen(ctrl_pass));
	//printf("---------------send Start\n");
	digit += 32;
	
	Web_Send_Data(digit);
}

// 修改密码信息
void change_login_info(char *name,char *old_pass,char *new_pass)
{
	int digit = 16;
	set_Head_Data(21,3,0,0);
	memcpy(web_send_buf + digit,name,32);
	//Name_padding(digit + strlen(name),32 - strlen(name));
	digit += 32;
	memcpy(web_send_buf + digit,old_pass,32);
	//Name_padding(digit + strlen(name),32 - strlen(old_pass));
	digit += 32;
	memcpy(web_send_buf + digit,new_pass,32);
	Name_padding(digit + strlen(new_pass),32);
	digit += 32;
	
	Web_Send_Data(digit);
}

//forget password
void forget_password(char *name,char *pass,char *ctrl_pass)
{
	int digit = 16;
	set_Head_Data(21,4,0,0);
	memcpy(web_send_buf + digit,name,32);
	Name_padding(digit + strlen(name),32 - strlen(name));
	digit += 32;
	memcpy(web_send_buf + digit,pass,32);
	Name_padding(digit + strlen(pass),32 - strlen(pass));
	digit += 32;
	memcpy(web_send_buf + digit,ctrl_pass,32);
	//Name_padding(digit + strlen(name),32 - strlen(name));
	digit += 32;
	Web_Send_Data(digit);
	
}
//获取设备信息
void get_all_status(int type,int option)
{
	int digit = 16;
	set_Head_Data(type , 0 , 0 , 0);
	Int_to_char(digit,option);
	digit += 4;
	Web_Send_Data(digit);
}	
//基本控制
void basic_contrl(int type,int id,int port,int status)
{
	int digit = 16;
	set_Head_Data(type,1,0,0);
	Int_to_char(digit,id);
	digit += 4;
	Int_to_char(digit,port);
	digit += 4;
	Int_to_char(digit,status);
	digit += 4;
	Web_Send_Data(digit);
}

//门锁控制
void door_contrl(int id,int option,char *pass)
{
	int digit = 16;
	switch(option)
	{
		case 0x24:
		case 0x25:
		case 0x26:
			set_Head_Data(52,0,0,0);
			break;
		case 0x27:
			set_Head_Data(53,0,0,0);
			break;
		case 0x28:
			set_Head_Data(55,0,0,0);
			break;
	}
	Int_to_char(digit,id);
	digit += 4;
	Int_to_char(digit,option);
	digit += 4;
	memcpy(web_send_buf + digit,pass,6);
	digit += 6;
	Web_Send_Data(digit);
}
//背景音乐请求数据
void Get_Music_Data(int type,int request_type,int id,int data_type)
{
	int digit = 16;
	set_Head_Data(type,request_type,0,0);
	Int_to_char(digit,id);
	digit += 4;
	Int_to_char(digit,data_type);
	digit += 4;
	Web_Send_Data(digit);
}

//背景音乐基本操作
void Music_Contrl(int type,int id,int option_type,int cmd1,int cmd2,int data1,int data2)
{
	int digit  = 16;
	set_Head_Data(65,type,0,0);
	Int_to_char(digit,id);
	digit += 4;
	Int_to_char(digit,option_type);
	digit += 4;
	Int_to_char(digit,cmd1);
	digit += 4;
	Int_to_char(digit,cmd2);
	digit += 4;
	Int_to_char(digit,data1);
	digit += 4;
	Int_to_char(digit,data2);
	digit += 4;
	Web_Send_Data(digit);
} 

//设置数据头部
void set_Head_Data(int type,int total , int note1, int note2)
{
	memset(web_send_buf,0,SEND_BUFSIZE);
	Int_to_char(0 , type);				//标志位
	Int_to_char(4 , total);				//辅助位
	Int_to_char(8 , note1);			//预留位
	Int_to_char(12 , note2);		//预留位
}

//自定义数据类型装换
void Int_to_char(int location , int data)
{
	char char_data[4] = "";
	int i = 0;
	int j = 1000;
	for(i = 0 ; i < 4 ; i++)
	{
		char_data[i] = data / j + 48 ;
		data %= j;
		j /= 10;
	}
	//拷贝到发送数组中
	memcpy(web_send_buf + location , char_data , 4);
}

//名字补充
void Name_padding(int location , int length)
{
	// char padding[] = "                                ";
	//char padding[] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
	char padding[] = "00000000000000000000000000000000";
	
	memcpy(web_send_buf + location , padding , length);
}