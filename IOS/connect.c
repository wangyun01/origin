#include "connect.h"

//连接服务器
int client_connect()
{
	//创建
	int listenfd = socket(AF_INET,SOCK_STREAM,0);
	if(listenfd < 0)
		ERR("socket");
	//邦定
	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	//邦定端口
	server_addr.sin_port = htons(WEB_PORT);;
	//帮定IP
	server_addr.sin_addr.s_addr = inet_addr(WEB_SERVER_IP);
	memset(server_addr.sin_zero,0,8);
	//连接服务器
	if(connect(listenfd,(struct sockaddr*)&server_addr,sizeof(struct sockaddr_in)) < 0)
		ERR("connect FAILED");
	
#ifdef CONNECT
	printf("connect successd\n");
#endif

	return listenfd;
}

//关闭通信套接字
void client_close(int listenfd)
{
	close(listenfd);
}
