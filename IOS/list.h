#ifndef LIST_H
#define LIST_H

//typedef char T;
typedef struct node Node;
typedef struct list List;

typedef struct Data
{
	int length;
	char content[2048];
}T;

struct node
{
	T data;		//数据
	struct node *next;	//指向下一个节点的指针
};

struct list
{
	struct node *pHead;	//指向头结点的指针
	int size;	//数据的个数
};

//本例中：头结点不存放数据

//创建一个空链表
List *listCreate();
//销毁整个链表
void listDestroy(List *pList);
//清空链表
int listClear(List *pList);
//添加数据(往后)
int listPushBack(List *pList,T data);
//添加数据(往前)
int listPushFront(List *pList,T data);
//添加数据(往指定的pos位置)
int listInsert(List *pList,T data,int pos);
//删除数据(最后)
int listPopBack(List *pList);
//删除数据(最前)
int listPopFront(List *pList);
//删除数据(指定的pos位置)
int listDel(List *pList,int pos);
//查找数据(依据指定的pos位置查询，结果由pData结果参数返回)
int listFind(List *pList,int pos,T *pData);
//获取链表中数据的个数
int listSize(List *pList);
//是否为空
int listEmpty(List *pList);

//链表中是否有指定的元素，有返回1,否则返回0
int listHas(List *pList,T data);
//删除链表中所有的data元素
int listDelByData(List *pList,T data);

#endif
