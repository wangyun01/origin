#ifndef __WEB_DEAL__H__
#define __WEB_DEAL__H__


#include "common.h"
#include "list.h"
//#include "send.h"
//#include "analy_data.h"

#define MAX_NUM 128
#define MIN_NUM	6

typedef struct
{
	int id;
	char name[13];
	char area[13];
	int type;
	int Contype;
	int use;
}Group_Data;

typedef struct 
{
	int id;
	int number;
	int port[15];
	char name[15][13];
	char area[15][13];
	int status[15];
	int use[15];
}Light_Data;

typedef struct
{
	int id;
	char name[13];
	char area[13];
	int use;
}Curtain_Data;

typedef struct
{
	int id;
	char name[13];
	char area[13];
	int status;
	int use;
}Socket_Data;

typedef struct
{
	int infrared_id;
	int app_id;
	int app_type;
	char name[13];
	char area[13];
	int use;
}App_Data;

typedef struct 
{
	int id;
	int power;
	int status;
	char name[13];
	char area[13];
	int use;
}Door_Data;

typedef	struct 
{
	int status[MAX_NUM];
	int power[MAX_NUM];
}Door_Status;

typedef struct 
{
	
}Sensor_Data;

typedef struct
{
	int id;
	char name[15];
	char area[15];
}Audio_Data;

typedef struct
{
	int id;
	int power;
	int play_status;
	int vol;
	int play_mode;
	int play_style;
	char current_music[105]; 
}Audio_Status;

typedef struct
{
		
}Music_Data;

typedef struct
{
	int id;
	char name[15];
	char area[15];			
	
}Solenoid_Valev;




//组合模式
void analy_group(char *buf);
//灯信息
void analy_light(char *buf);
//继电器状态
void analy_relay_status(char *buf);

//窗帘
void analy_curtain(char *buf);

//插座
void analy_socket(char *buf);

//插座状态
void analy_socket_status(char *buf);

//门锁
void analy_door(char *buf);

//门锁状态
void analy_door_status(char *buf);

//家电信息
void app_analy(char *buf);

//背景音乐基本信息
void analy_audio(char *buf);

//电磁阀信息
void analy_solenoid_valev(char *buf);
//电磁阀状态
void solenoidvalev_status(char *buf);

//初始化缓存空间
void init_device();
//处理队列数据
void *deal_data();
//数据转换char to int 
int Char_to_int(char *data);
 //数据转换short to int 
int Short_to_int(char *data);
//单个字符转换 int 
int Char_to_Byte(char *data);


void *Can_Rx_Thread();


void group_data_up(int num,int id,char *name,char *area,int type,int use);
void light_data_up(int num,int use_num,int id,int port,char *name,char *area,int type,int use);
void curtain_data_up(int num,int id,char *name,char *area,int use);
void socket_data_up(int num,int id,char *name,char *area,int use);
void music_data_up(int num,int id,char *name,char *area);
void door_data_up(int num,int id,char *name,char *area,int use);
void solenoid_valev_data_up(int num,int id ,char *name,char *area);
void app_data_up(int num,int infrared_id,int id,char *name,char *area,int type,int use);


#endif 