#ifndef __ASE_OPTIONS__H
#define __ASE_OPTIONS__H

#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/aes.h>

void aes_init();	//线程锁
void aes_encode(char *sourcestr, char **result, int length);	//数据加密
void aes_decode(char *crypttext, char **out ,int length);	//数据解密
#endif
