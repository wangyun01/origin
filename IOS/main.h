#ifndef __MAIN__H__
#define __MAIN__H__


#include "common.h"
//缓存
//#include "device.h"
//队列
//#include "queue.h"
#include "list.h"
//数据加密
#include "aes_option.h"
#include "receive.h"
#include "web_deal.h"
#include "connect.h"


//初始化所有内容
void init();
//初始化登陆信息存储
void init_login();
//初始化缓存
void init_cache();
//初始化数据可
void init_sql();
//队列
void init_queue();
//数据加密初始化
void init_aes();
//连接网络
void connect_web();
//线程
void open_phread(List *list);


#endif