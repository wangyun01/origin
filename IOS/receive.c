#include "receive.h"


#define REC

extern int client_fd;
char web_rec_buf[REC_BUFSIZE] = "";

void *receive_data(void *arg)
{
	List *list = arg;
	while(1)
	{
		if(sock_alive(client_fd))
		{
			memset(web_rec_buf, 0 , REC_BUFSIZE);
			
			int count = 0;
			if((count = read(client_fd, web_rec_buf, REC_BUFSIZE)) < 0)
			{
				ERR("receive err");
			}
			printf("receive_buf:%s\n",web_rec_buf);
			//接受的数据进行解密
			char *decrypt_str = NULL;
			aes_decode(web_rec_buf,&decrypt_str ,count);
			//printf("count:%d\n",count);
			//printf("decrypt:%s\n",decrypt_str);
			//入队
			if(NULL != list)
			{
				//填充数据空间
				T elem;
				memset(&elem, 0 , sizeof(T));
				//elem.length = count;
				memcpy(elem.content, decrypt_str, count);
				printf("content:%s\n",elem.content);
				printf("count:%d\n",count);
				//elem.length = count;
				//插入队列
				//queuePush(queue,elem);
				listPushBack(list,elem);
			}
			//释放数据加密申请的空间
			 free(decrypt_str);
		}
		
		else
		{
			//printf("socket die!\n");
			sleep(1);
			continue;
		}
		
	}
	
	
	
}

//判断是否活着
int sock_alive(int fd){

	int alive,idle,intvl,cnt;
    alive = 1;		// 开启alive属性
    idle =IDLE;

    int ret=1;
    if(setsockopt(fd,SOL_SOCKET,SO_KEEPALIVE,&alive,sizeof(alive))!=0)
	{
        ret=0;
    }
    if(setsockopt(fd,SOL_TCP,TCP_KEEPIDLE,&idle,sizeof(idle))!=0)
	{
        ret=0;
    }
    intvl=INTVL;
    cnt=CNT;
    if(setsockopt(fd,SOL_TCP,TCP_KEEPINTVL,&intvl,sizeof(intvl))!=0)
	{
        ret=0;
    }
    if(setsockopt(fd,SOL_TCP,TCP_KEEPCNT,&cnt,sizeof(cnt))!=0)
	{
        ret=0;
    }
    return ret;

}