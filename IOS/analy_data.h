#ifndef __ANALY__DATA__H__
#define __ANALY__DATA__H__

//#include "common.h"
//#include "queue.h"
#include "web_deal.h"


#define MAX_NUM 128
#define MIN_NUM	6

typedef struct
{
	int id;
	char name[13];
	char area[13];
	int type;
	int Contype;
	int use;
}Group_Data;

typedef struct 
{
	int id;
	//int number;
	int port[15];
	char name[15][13];
	char area[15][13];
	int status[15];
	int use[15];
}Light_Data;

typedef struct
{
	int id;
	char name[13];
	char area[13];
	int use;
}Curtain_Data;

typedef struct
{
	int id;
	char name[13];
	char area[13];
	int status;
	int use;
}Socket_Data;

typedef struct
{
	int infrared_id;
	int app_id;
	int app_type;
	char name[13];
	char area[13];
	int use;
}App_Data;

typedef struct 
{
	int id;
	int power;
	int status;
	char name[13];
	char area[13];
	int use;
}Door_Data;

typedef	struct 
{
	int status[MAX_NUM];
	int power[MAX_NUM];
}Door_Status;

typedef struct 
{
	
}Sensor_Data;

typedef struct
{
	int id;
	char name[15];
	char area[15];
}Audio_Data;

typedef struct
{
	int id;
	int power;
	int play_status;
	int vol;
	int play_mode;
	int play_style;
	char current_music[105]; 
}Audio_Status;

typedef struct
{
		
}Music_Data;

//数量统计
//int light_num;




//模式
Group_Data group_data[MAX_NUM];
//灯
Light_Data light_data[MAX_NUM];
//窗帘
Curtain_Data curtain_data[MAX_NUM];
//插座
Socket_Data socket_data[MAX_NUM];
//家电
App_Data app_data[MAX_NUM];
//门
Door_Data door_data[MIN_NUM];
//传感器
Sensor_Data sensor_data[MIN_NUM];

//背景音乐信息
Audio_Data audio_data[MIN_NUM];

//背景音乐状态
Audio_Status audio_status;

//窗帘状态
int curtain_status[MAX_NUM];

//继电器状态
int relay_status[MAX_NUM];

//门锁状态
Door_Status door_status[MAX_NUM];

//固定模式使用
int mode_status[MAX_NUM];

int socket_status[MAX_NUM];



//组合模式
void analy_group(char *buf);
//灯信息
void analy_light(char *buf);
//继电器状态
void analy_relay_status(char *buf);

//窗帘
void analy_curtain(char *buf);

//插座
void analy_socket(char *buf);

//插座状态
void analy_socket_status(char *buf);

//门锁
void analy_door(char *buf);

//门锁状态
void analy_door_status(char *buf);

//家电信息
void app_analy(char *buf);


#endif