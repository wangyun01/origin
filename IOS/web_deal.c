#include "web_deal.h"

//模式
Group_Data group_data[MAX_NUM];
//灯
Light_Data light_data[MAX_NUM];
//窗帘
Curtain_Data curtain_data[MAX_NUM];
//插座
Socket_Data socket_data[MAX_NUM];
//家电
App_Data app_data[MAX_NUM];
//门
Door_Data door_data[MIN_NUM];
//传感器
Sensor_Data sensor_data[MIN_NUM];

//背景音乐信息
Audio_Data audio_data[MIN_NUM];

//背景音乐状态
Audio_Status audio_status;

//窗帘状态
int curtain_status[MAX_NUM];

//继电器状态
int relay_status[MAX_NUM];

//门锁状态
Door_Status door_status[MAX_NUM];

Solenoid_Valev solenoid_valev[MAX_NUM];



//固定模式使用
int mode_status[MAX_NUM];

int socket_status[MAX_NUM];

//电磁阀状态
int solenoid_valev_status[MAX_NUM];

//初始化缓存空间
void init_device()
{
	//不写了
}

//处理队列数据
void *deal_data(void *arg)
{

	List *list = arg;
	while(1)
	{
		if(NULL == list)
		{
			//printf("queue NULL\n");
			usleep(100);
			continue;
		}
		 				
		// 解析数据
		T data_buf;
		memset(&data_buf,0,sizeof(T));
		// queuePop(queue,&data_buf);
		 char *buf = NULL;
		if(!listEmpty(list))
		{
			listFind(list,0,&data_buf);
			//printf("===============================================,buf\n");
			buf = data_buf.content;
			//printf("/////////////////////////data:%s\n",buf);
			listPopFront(list);
			
		
		
			//printf("??????????????? buf:%s\n",buf);
			//sleep(1);
			int operate_opt = Char_to_int(buf);
			int user_option = Char_to_int(buf + 16);
			//printf("??????????????? buf:%d\n",operate_opt);
		
			switch(operate_opt)
			{
				case 21:  //验证密码
					switch(user_option)
					{
						case 0:
							printf("login successful!\n");
							get_all_status(22,7);
							 // get_music_msg();
							break;
							
						case 1:
							printf("用户信息有误(Login ERR)\n");
							break;
						default:
							break;
					}
					break;
				case 25:    						
							//	获取所有状态信息
					 analy_relay_status(buf);//继电器状态
					 break;
				case 48:								
					analy_socket_status(buf);//插座状态
					break;
				case 54:								
					analy_door_status(buf);//门锁状态
					break;
				case 70:
					solenoidvalev_status(buf);
				case 28:
										
					analy_group(buf);	//组合模式
					break;
				case 27:   //灯信息
					
					
					analy_light(buf);
					
					break;
				case 47:   //窗帘
						
					analy_curtain(buf);
					break;
				case 49://插座
					
					analy_socket(buf);
					break;
				case 51://门锁状态
					
					analy_door(buf);
					break;
				case 46:  //家电信息
					
					app_analy(buf);
					break;
				case 60:
					analy_audio(buf);
					break;
				case 62:
					printf("Music Data::::::::::::::::%s",buf);
					
				case 71:
					analy_solenoid_valev(buf);
					break;
				default:
					break;
			}
 		}
		else
		{
			//printf("***************List NULL**************\n");
			continue;
		}
	}
 }
 
 /***************************数据类型转换********************************/
int Char_to_int(char *data)
{
	
	int i = 0;
	int j = 1000;
	int int_data = 0 ;
	for(i = 0 ; i < 4 ; i++)
	{
		
		int_data += (*(data + i) - 48) * j;
		j /= 10;
	}
	//int_data = atoi(data);
	 return int_data;
	
}

int Short_to_int(char *data)
{
	int result = 0 ;
	result = (data[0] - 48) * 10 + (data[1] - 48);
	return result;
}

int Char_to_Byte(char *data)
{
	int int_data = 0;
	char  *char_data = data;
	int_data = *char_data - 48;
	return int_data;
}

/*****************************数据解析*********************************/

//组合模式
void analy_group(char *buf)
{
	int i = 0,j = 0,digit = 26;
	int group_num = Char_to_int(buf + 4);
	//printf("====num_group%d ============\n",group_num);
	printf("===================GROUP=======================\n");
	//10种固定模式的使用
	for(j = 0;j < 10; j++ )
	{
		memset(&mode_status,0,sizeof(mode_status));
		mode_status[j] = Char_to_Byte(buf + j);
		printf("mode:%d\n",mode_status[j]);	
	}
	for(i = 0;i < group_num - 10;i++)
	{
		memset(&group_data,0,sizeof(group_data));
		group_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(group_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(group_data[i].area,buf + digit,area_len);
		digit += 12;
		group_data[i].type = Char_to_int(buf + digit);
		digit += 4;
		group_data[i].Contype = Char_to_int(buf + digit);
		digit += 4;
		printf("num : %d\tgroup_id:%d\tname:%s,area:%s,type:%d,Contype:%d\n",i ,group_data[i].id,group_data[i].name,group_data[i].area,group_data[i].type,group_data[i].Contype);	
	}
	
}

//灯信息
void analy_light(char *buf)
{
	int j=0,digit = 16;
	
	
	int light_num = Char_to_int(buf + 4);
	light_data[light_num].number = light_num;
	//printf("relay_num: %d,use_num :%d\n",relay_num,use_num);
	
	memset(&light_data[light_num],0,sizeof(light_data));
	light_data[light_num].id = Char_to_int(buf + digit);
	digit += 4;
	int use_num = Char_to_int(buf + digit);
	digit += 4;
	for(j = 0;j < use_num;j++)
	{
		light_data[light_num].port[j] = Char_to_int(buf + digit);
		digit += 4;
		
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		//printf("===========================================\n");
		memcpy(light_data[light_num].name[j] , buf + digit, name_len);
		printf("===================LIGHT=======================\n");
		digit += 12;
		
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(light_data[light_num].area[j] , buf + digit, area_len);
		digit += 12;
		printf("id %d port:%d  name:%s area:%s \n",light_data[light_num].id,light_data[light_num].port[j],light_data[light_num].name[j],light_data[light_num].area[j]);
		//light_num++;	
	
	}	
				
}
//继电器状态
void analy_relay_status(char *buf)
{
	printf("==========================================\n");
	int i = 0;
	int use_num = Char_to_int(buf + 4);
	for(i = 0 ;i < use_num;i++)
	{
		relay_status[i] = Char_to_Byte(buf + 16 + i);
	
	
	printf("relay_sta:%d\n",relay_status[i]);	

	}
}


//窗帘
void analy_curtain(char *buf)
{
	int i = 0,digit = 16;
	int curtain_num = Char_to_int(buf + 4);
printf("===================CURTAIN=======================\n");
	for(i = 0;i < curtain_num;i++)
	{
		curtain_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(curtain_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(curtain_data[i].area,buf + digit,area_len);
		digit += 12;
		printf("curtain_id:%d,name:%s,area:%s\n",\
		curtain_data[i].id,curtain_data[i].name,curtain_data[i].area);	
	}
	
}
//插座
void analy_socket(char *buf)
{
	printf("===================SOCKET=======================\n");
	int i = 0 ,digit = 16;
	int socket_num = Char_to_int(buf + 4);
	for(i = 0;i < socket_num; i++)
	{
		
		socket_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(socket_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(socket_data[i].area,buf + digit,area_len);
		digit += 12;
		printf("socket_id:%d,name:%s,area:%s\n",\
		socket_data[i].id,socket_data[i].name,socket_data[i].area);	
	}
	
}
//插座状态
void analy_socket_status(char *buf)
{
	printf("==========================================\n");
	int socket_num  = Char_to_int(buf + 4);
	int i = 0;
	for(i = 0;i < socket_num;i++)
	{
		socket_status[i] = Char_to_Byte(buf + 16 + i);
		printf("socket_sta:%d\n",socket_status[i]);
	}
	
}
//门锁
void analy_door(char *buf)
{
	int i = 0,digit = 16;
	//Door_Status door;
	int num_safelock = Char_to_int(buf + 4);
	
	printf("===================DOOR=======================\n");
	for(i = 0; i < num_safelock;i++)
	{
		door_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(door_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + 34);
		digit += 2;
		memcpy(door_data[i].area,buf + digit,area_len);
		printf("door_id:%d,name:%s,area:%s\n",\
		door_data[i].id,door_data[i].name,door_data[i].area);	
	}
	
	
}
//门锁状态
void analy_door_status(char *buf)
{
	printf("==========================================\n");
	int safe_lock_num = Char_to_int(buf + 4 );
	int i = 0,j = 0;
	for(i = 0;i < safe_lock_num;i++)
	{
		for(j = 0;j< i;j++)
		{
			door_status[i].status[j] = Char_to_Byte(buf + (16 + j));
			door_status[i].power[j] = Char_to_Byte(buf + (17 + j));
			j+=2;
			printf("door_sta:%d,door_power:%d\n",door_status[i].status[j],door_status[i].status[j]);
		}
	}
		
}
//家电信息
void app_analy(char *buf)
{
	int j = 0,digit = 16;
	int App_num = Char_to_int(buf + 4);
	if (App_num == 0)
		printf("无此类信息\n");
	printf("===================APPLIANCE=======================\n");
	
	int app_count = Char_to_int(buf + 4);
	
	if(app_count != 0)
	{
		memset(&app_data[j],0,sizeof(app_data));
		//printf("~~~~~~~~~~~~~~app_count:%d\n",app_count);
		for(j = 0;j < app_count; j++)
		{
			//printf("~~~~~~~~~~~~~~app_num:%d\n",App_num);
			
			app_data[j].infrared_id = Char_to_int(buf + digit);
			digit += 4;
			app_data[j].app_id = Char_to_int(buf + digit);
			digit += 4;
			app_data[j].app_type = Char_to_int(buf + digit);
			digit += 4;
			int name_len = Short_to_int(buf + digit);
			digit += 2;
			memcpy(app_data[j].name,buf + digit,name_len);
			digit += 12;
			int area_len = Short_to_int(buf + digit);
			digit += 2;
			memcpy(app_data[j].area,buf + digit,area_len);
			digit += 12;
			printf("infrared_id:%d,app_id:%d,type:%d,name:%s,area:%s\n",app_data[j].infrared_id,app_data[j].app_id,app_data[j].app_type,app_data[j].name,app_data[j].area);
		}
		
		
		//digit = 16;
	}
}

//背景音乐信息
void analy_audio(char *buf)
{
	int i = 0,digit = 16;
	int audio_num = Char_to_int(buf + 4);
	
	
	printf("===================MUSIC INFO=======================\n");
	for(i = 0;i < audio_num;i++)
	{
		audio_data[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(audio_data[i].name,buf + digit,name_len);
		digit += 12;
		int area_len = Short_to_int(buf + digit);
		digit += 2;
		memcpy(audio_data[i].area,buf + 36,area_len);
		digit += 12;
		printf("audio_id:%d,name:%s,area:%s\n",audio_data[i].id,audio_data[i].name,audio_data[i].area);
	}
	
}

//背景音乐状态
void analy_audio_status(char *buf)
{
	printf("==========================================\n");
	int digit = 16;
	audio_status.id = Char_to_int(buf + digit);
	digit += 4;
	audio_status.power = Char_to_int(buf + digit);
	digit += 4;
	audio_status.play_status = Char_to_int(buf + digit);
	digit += 4;
	audio_status.vol = Char_to_int(buf + digit);
	digit += 4;
	audio_status.play_mode = Char_to_int(buf + digit);
	digit += 4;
	audio_status.play_style = Char_to_int(buf + 36);
	digit += 4;
	memcpy(audio_status.current_music,buf + digit,100);
	digit += 100;
}

//电磁阀信息
void analy_solenoid_valev(char *buf)
{
	printf("===================SOLENOID VALVE=======================\n");
	int digit = 16 , i = 0;
	int num = Char_to_int(buf + 4);
	for(i = 0;i < num; i++)
	{
		solenoid_valev[i].id = Char_to_int(buf + digit);
		digit += 4;
		int name_len = Short_to_int( buf+ digit);
		digit += 2;
		memcpy(solenoid_valev[i].name,buf + digit,name_len);
		digit += 12;
		name_len = Short_to_int(buf + digit);
		digit += 2; 
		memcpy(solenoid_valev[i].area,buf + digit,name_len);
		printf("solenoid_valev_id:%d,name:%s,area:%s\n",solenoid_valev[i].id,solenoid_valev[i].name,solenoid_valev[i].area);
	}
	
}
void solenoidvalev_status(char *buf)
{

	printf("==========================================\n");
	int digit = 16 , i = 0;
	int num = Char_to_int(buf + 4);
	for(i = 0;i < num;i++)
	{
		solenoid_valev_status[i] = Char_to_Byte(buf + digit);
		digit ++;
		printf("solenoid_valev_status:%d\n",solenoid_valev_status[i]);
	}
}


/**************************************数据读取********************************************/
//组合模式
void group_data_up(int num,int id,char *name,char *area,int type,int use)
{
	group_data[num].id = id;
	memcpy(group_data[num].name,name,strlen(name));
	memcpy(group_data[num].area,area,strlen(area));
	group_data[num].type = type;
	group_data[num].use = use;
}

//灯
void light_data_up(int num,int use_num,int id,int port,char *name,char *area,int type,int use)
{
	light_data[num].id = id;
	light_data[num].port[use_num] = port;
	memcpy(light_data[num].name[use_num],name,strlen(name));
	memcpy(light_data[num].area[use_num],area,strlen(area));
	//light_data[num].type = type;
	light_data[num].use[use_num] = use;
}

//窗帘
void curtain_data_up(int num,int id,char *name,char *area,int use)
{
	curtain_data[num].id = id;
	memcpy(curtain_data[num].name,name,strlen(name));
	memcpy(curtain_data[num].area,area,strlen(area));
	curtain_data[num].use = use;
}

//插座
void socket_data_up(int num,int id,char *name,char *area,int use)
{
	socket_data[num].id = id;
	memcpy(socket_data[num].name,name,strlen(name));
	memcpy(socket_data[num].area,area,strlen(area));
	socket_data[num].use = use;
}

//音乐
void music_data_up(int num,int id,char *name,char *area)
{
	audio_data[num].id = id;
	memcpy(audio_data[num].name,name,strlen(name));
	memcpy(audio_data[num].area,area,strlen(area));
}

//锁
void door_data_up(int num,int id,char *name,char *area,int use)
{
	door_data[num].id = id;
	memcpy(door_data[num].name,name,strlen(name));
	memcpy(door_data[num].area,area,strlen(area));
	door_data[num].use = use;
}

//电磁阀
void solenoid_valev_data_up(int num,int id ,char *name,char *area)
{
	solenoid_valev[num].id = id;
	memcpy(solenoid_valev[num].name,name,strlen(name));
	memcpy(solenoid_valev[num].area,area,strlen(area));
	
}

//家电
void app_data_up(int num,int infrared_id,int id,char *name,char *area,int type,int use)
{
	app_data[num].infrared_id = infrared_id;
	app_data[num].app_id = id;
	memcpy(app_data[num].name,name,strlen(name));
	memcpy(app_data[num].area,area,strlen(area));
	app_data[num].app_type = type;
	app_data[num].use = use;
}