#include "aes_option.h"


char* base64_encode(char* sourcestr,  int data_len ); 
char *base64_decode(char *data, int data_len, int *out_len); 
static char find_pos(char ch); 


const char base[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="; 
char key[] = "zsznhomeservices";

pthread_mutex_t aes_encode_mutex;
pthread_mutex_t aes_decode_mutex;


//初始化线程
void aes_init()
{
	pthread_mutex_init(&aes_encode_mutex,NULL);
	pthread_mutex_init(&aes_decode_mutex,NULL);
}

void aes_encode(char *sourcestr, char **result, int length)
{
  //  if (strcmp(key, "") == 0) key = aeskey;
	//上锁
	pthread_mutex_lock(&aes_encode_mutex);
    int len = length;
    unsigned char iv[AES_BLOCK_SIZE+1] = "www.zszn-tec.com";

	unsigned char * out = NULL;
	out = (unsigned char *)malloc(1024 * 1024);
    if (out == NULL) 
	{
        fprintf(stderr, "No Memory!\n");
    }
    AES_KEY aes = {{0} , 0};
    if(AES_set_encrypt_key((unsigned char*)key, 128, &aes) < 0)
    {
        return;
    }

    /* 计算补0后的长度 */
    int out_len = ((len ) / 16 + 1)* 16;
    char * sstr = (char *)malloc(sizeof(char) * (out_len + 1));
    /* 补0 */
    memset(sstr, 0, out_len+1);
    strcpy(sstr, sourcestr);
	
    AES_cbc_encrypt((unsigned char*)sstr, out, out_len, &aes, (unsigned char*)iv, AES_ENCRYPT);
    /* 这里的长度一定要注意，不能用strlen来获取，加密后的字符串中可能会包含\0 */
   *result = base64_encode((char *)out, out_len);

	free(out);
    free(sstr);
	
	pthread_mutex_unlock(&aes_encode_mutex);
}

void aes_decode(char *crypttext,char **out ,int length)
{
 //   if (strcmp(key, "") == 0) key = aeskey;
	
	pthread_mutex_lock(&aes_decode_mutex);
    int *out_len = (int *)malloc(sizeof(int *));
    unsigned char iv[AES_BLOCK_SIZE + 1] = "www.zszn-tec.com";

  //  char *in = base64_decode(crypttext, strlen(crypttext), out_len);
	char *in = NULL;
	in = base64_decode(crypttext, length, out_len);
	
	if(in == NULL)
	{
		pthread_mutex_unlock(&aes_decode_mutex);
		return ;
	}
		
	*out = (char *)malloc(sizeof(char *) * (*out_len + 1));
  //  char *out = (char *) malloc(sizeof(char *) * (*out_len) + 1);
    memset(*out, 0, *out_len + 1);
    AES_KEY aes = {{0} , 0};
    if(AES_set_decrypt_key((unsigned char*)key, 128, &aes) < 0)
    {
		pthread_mutex_unlock(&aes_decode_mutex);
        return ;
    }
    AES_cbc_encrypt((unsigned char*)in, (unsigned char*)*out, *out_len, &aes, (unsigned char*)iv, AES_DECRYPT);
	free(out_len);
	free(in);
//    return out;
	pthread_mutex_unlock(&aes_decode_mutex);
}

char *base64_encode(char *data, int data_len)
{
    int prepare = 0;
    int ret_len = 0;
    int temp = 0;
    char *ret = NULL;
    char *f = NULL;
    int tmp = 0;
    char changed[4];
    int i = 0;
    ret_len = data_len / 3;
    temp = data_len % 3;
    if (temp > 0)
    {
		ret_len += 1;
    }
    ret_len = ret_len*4 + 1;
    ret = (char *)malloc(ret_len * sizeof(char *));
    if ( ret == NULL)
    {
		printf("No enough memory.\n");
		exit(0);
    }
    memset(ret, 0, ret_len);
    f = ret;
	
    while (tmp < data_len)
    {
		temp = 0;
		prepare = 0;
		memset(changed, '\0', 4);
		while (temp < 3)
		{
			//printf("tmp = %d\n", tmp);
			if (tmp >= data_len)
			{
				break;
			}
			prepare = ((prepare << 8) | (data[tmp] & 0xFF));
			tmp++;
			temp++;
		}
		prepare = (prepare<<((3-temp)*8));
		//printf("before for : temp = %d, prepare = %d\n", temp, prepare);
		for (i = 0; i < 4 ;i++ )
		{
			if (temp < i)
			{
			changed[i] = 0x40;
			}
			else
			{
				changed[i] = (prepare>>((3-i)*6)) & 0x3F;
			}
	//		*f = base[changed[i]];
			strncpy(f,base + changed[i],1);
		//	printf("%.2X", changed[i]);
			f++;
		}
    }
    *f = '\0';
    return ret;
}

/* */ 
static char find_pos(char ch)   
{ 
    char *ptr = (char*)strrchr(base, ch);//the last position (the only) in base[] 
    return (ptr - base); 
} 
/* */ 


/* out_len 解码后的数据长度 */
char *base64_decode(char *data, int data_len, int *out_len)
{
    int ret_len = (data_len / 4) * 3;
    int equal_count = 0;
    char *ret = NULL;
    char *f = NULL;
    int tmp = 0;
    int temp = 0;
    char need[3];
    int prepare = 0;
    int i = 0;
    if (*(data + data_len - 1) == '=')
    {
		equal_count += 1;
    }
    if (*(data + data_len - 2) == '=')
    {
		equal_count += 1;
    }
    if (*(data + data_len - 3) == '=')
    {//seems impossible
		equal_count += 1;
    }
    switch (equal_count)
    {
		case 0:
			ret_len += 4;//3 + 1 [1 for NULL]
			break;
		case 1:
			ret_len += 4;//Ceil((6*3)/8)+1
			break;
		case 2:
			ret_len += 3;//Ceil((6*2)/8)+1
			break;
		case 3:
			ret_len += 2;//Ceil((6*1)/8)+1
			break;
    }
    ret = (char *)malloc(ret_len * sizeof(char *));
    if (ret == NULL)
    {
		printf("No enough memory.\n");
		exit(0);
    }
    memset(ret, 0, ret_len);
    f = ret;
    while (tmp < (data_len - equal_count))
    {
	temp = 0;
	prepare = 0;
	memset(need, 0, 3);
	while (temp < 4)
	{
	    if (tmp >= (data_len - equal_count))
	    {
		break;
	    }
	    prepare = (prepare << 6) | (find_pos(data[tmp]));
	    temp++;
	    tmp++;
	}
	prepare = prepare << ((4-temp) * 6);
	for (i=0; i<3 ;i++ )
	{
	    if (i == temp)
	    {
		break;
	    }
	    *f = (char)((prepare>>((2-i)*8)) & 0xFF);
	    f++;
	}
    }
    *f = '\0';
    *out_len = (f - ret);
    if (*out_len < 0) *out_len = 0;
    return ret;
}

