#ifndef __CONNECT_H__
#define __CONNECT_H__

#include "common.h"
//#define		WEB_SERVER_IP		"192.168.1.111"
#define		WEB_SERVER_IP		"192.168.1.90"			//内网服务器
//#define		WEB_SERVER_IP		"182.92.241.25"		//外网服务器
#define     WEB_PORT        	 4560

//连接服务器
int client_connect(); //ip 端口

//关闭通信套接字
void client_close(int listenfd);		//关闭的通信套接字

#endif